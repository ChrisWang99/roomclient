describe("Home page", () => {
  beforeEach(() => {
    cy.visit("/#");
  });

  it("Shows a header", () => {
    cy.get(".vue-title").should("contain", "Room controller");
  });

  describe("Navigation bar", () => {
    it("Shows the required links", () => {
      cy.get(".navbar-nav")
        .eq(0)
        .within(() => {
          cy.get(".nav-item")
            .eq(0)
            .should("contain", "Home");
          cy.get(".nav-item")
            .eq(1)
            .should("contain", "Rooms List");
          cy.get(".nav-item")
            .eq(2)
            .should("contain", "Modify Room");
        });
      cy.get(".navbar-nav")
        .eq(1)
        .within(() => {
          cy.get(".nav-item")
            .eq(0)
            .should("contain", "About Us");
          cy.get(".nav-item")
            .eq(1)
            .should("contain", "Contact Us");
        });
    });

    it("Redirects when links are clicked", () => {
      cy.get("[data-test=roomsbtn]").click();
      cy.url().should("include", "/rooms");
      cy.get(".navbar-nav")
        .eq(0)
        .find(".nav-item")
        .eq(1)
        .click();
      cy.url().should("include", "/rooms");
      // etc
    });
  });
});
