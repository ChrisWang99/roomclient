const apiURL = "https://roomapi-staging.herokuapp.com/rooms/"

describe("Add room page", () => {
  beforeEach(() => {
    cy.request(apiURL)
      .its("body")
      .then(rooms => {
        rooms.forEach(element => {
          cy.request("DELETE", `${apiURL}${element.roomNumber}`);
        });
      });
    cy.fixture("rooms").then(rooms => {
      let [d1, d2, d3, d4, ...rest] = rooms;
      let four = [d1, d2, d3, d4];
      four.forEach(room => {
        cy.request("POST", apiURL, room);
      })
    })
    cy.visit("/")
    cy.get(".navbar-nav")
      .eq(0)
      .find(".nav-item")
      .eq(2)
      .click()
  })

  describe("Add a room", () => {
    describe("With valid attributes", () => {
      it("allows room to be submitted", () => {
        //  Fill out web form
        cy.get("#roomtype").select("single room")
        cy.get('input[data-test=price]').type(45)
        cy.get("input[data-test=roomNumber]").type(321)
        cy.get("#available").select("available")
        cy.get("label")
          .contains("Message")
          .next()
          .type("good one");
        cy.contains("Successful!").should("not.exist");
        cy.get(".error").should("not.exist");
        cy.get("button[type=submit]").click();
        cy.contains("Successful!").should("exist");
      });
      after(() => {
        cy.wait(100)
        // Click Manage Donations
        cy.get(".navbar-nav")
          .eq(0)
          .find(".nav-item")
          .eq(1)
          .click();
        cy.get("tbody")
          .find("tr")
          .should("have.length", 1);
      });
    });
    describe("With invalid/blank attributes", () => {
      it("shows error messages until all attributes are ", () => {
        cy.get("button[type=submit]").click();
        cy.get(".error").contains("Price");
        cy.get(".error").contains("Room number");
        cy.get(".error").contains("Message");
        cy.get("input[data-test=price]").type(321);
        cy.get("input[data-test=roomNumber]").type(321);
        cy.get(".error")
          .contains("Price")
          .should("not.exist");
        cy.get(".error")
          .contains("Room number")
          .should("not.exist");
        cy.get("label")
          .contains("Message")
          .next()
          .type("good one");
        cy.get(".error").should("not.exist");
      });
      after(() => {
        cy.wait(100)
        cy.get(".navbar-nav")
          .eq(0)
          .find(".nav-item")
          .eq(1)
          .click();
        cy.get("tbody")
          .find("tr")
          .should("have.length", 1);
      });
    });
  });
});
