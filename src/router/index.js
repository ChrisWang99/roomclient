/* eslint-disable */
import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home'
import Rooms from '../components/Rooms'
import MRoom from '../components/ModifyRoom'
import AboutUs from '../components/AboutUs'
import ContactUs from '../components/ContactUs'
import Edit from '../components/Edit'
import login from "../components/login"
import register from "../components/register"


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/rooms',
      name: 'Rooms',
      component: Rooms
    },
    {
      path: '/mroom',
      name: 'ModifyRoom',
      component: MRoom
    },
    {
      path: '/edit',
      name: 'Edit',
      component: Edit,
      props: true
    },
    {
      path: '/about',
      name: 'AboutUs',
      component: AboutUs
    },
    {
      path: '/contact',
      name: 'ContactUs',
      component: ContactUs
    },
    { path: '/login', name: 'login', component:login,props: true},
    { path: '/register', name: 'register', component:register,props: true},
  ]
})
