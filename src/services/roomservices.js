import Api from '../services/api'

export default {
  fetchRooms () {
    return Api().get('/rooms')
  },
  postRoom (room) {
    return Api().post('/rooms', room,
      { headers: {'Content-type': 'application/json'} })
  },
  deleteRoom (roomNumber) {
    return Api().delete(`/rooms/${roomNumber}`)
  },
  fetchRoom (roomNumber) {
    return Api().get(`/rooms/${roomNumber}`)
  },
  putRoom (roomNumber, room) {
    console.log('REQUESTING ' + room.roomNumber + ' ' +
      JSON.stringify(room, null, 5))
    return Api().put(`/rooms/${roomNumber}`, room,
      { headers: {'Content-type': 'application/json'} })
  }
}
