# Assignment 2 - Agile Software Practice.

Name: Fubo Wang. \
ID: 20086458

## Client UI.



![Image text](image/home.png) 
>>Home page of my projeect
![Image text](image/rl.png) 
>>room list
![Image text](image/addroom.png) 
>>add a room to room list
![Image text](image/log.png) 
>>login page


## E2E/Cypress testing.

(Optional) State any non-standard features (not covered in the lectures or sample code provided) of the Cypress framework that you utilized.

## Web API CI.

(Optional) State the GitLab Pages URL of the coverage report for your Web API tests, e.g.

https://gitlab.com/ChrisWang99/roomapi/tree/develop
## GitLab CI.

(Optional) State any non-standard features (not covered in the lectures or labs) of the GitLab CI platform that you utilized.
